#! /bin/sh

openssl req -newkey rsa:2048 -nodes -keyout ./docker/ssl/domain.key -x509 -days 3650 -out ./docker/ssl/domain.crt

openssl dhparam -out ./docker/ssl/dhparam.pem 2048